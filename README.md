
### Hi there 👋 :brazil:
I'm an adventurous developer that loves bots and data.

![](djif.gif)

  
## 🔗 Links


[![contact me](https://img.shields.io/badge/Tutanota-840010?style=for-the-badge&logo=Tutanota&logoColor=white)](mailto:minhadona@tutanota.de?subject=GitHub)
[![medium](https://img.shields.io/badge/Medium-12100E?style=for-the-badge&logo=medium&logoColor=white)](https://minhadona.medium.com/) 
  
![github stats](https://github-readme-stats.vercel.app/api?username=minhadona&show_icons=true&theme=merko)



## :robot: Skills
+ Python
+ Kafka
+ AWS
+ PostgreSQL
+ C
+ Java
+ Cobol
+ MySQL
+ Datadog
+ RethinkDB



## :heart_on_fire: Currently working with
+ Spark
+ Databricks
+ Azure
+ Airflow

![visitors](https://visitor-badge.laobi.icu/badge?page_id=minhadona)
